#!/bin/bash

THIS_DIR=$(realpath $(dirname -- "$0"))
TO_LOAD="\
    $THIS_DIR/conf.sway\
    $THIS_DIR/theme.sway\
    $THIS_DIR/bar/bar.sway\
    $THIS_DIR/lock/lock.sway\
    $THIS_DIR/controls.sway"

for FILE in $TO_LOAD
do
    echo "include $FILE"
done
