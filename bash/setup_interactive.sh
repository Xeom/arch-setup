#!/bin/bash

setup_shell()
{
    # don't put duplicate lines or lines starting with space in the history.
    # See bash(1) for more options
    HISTCONTROL=ignoreboth

    # append to the history file, don't overwrite it
    shopt -s histappend

    # check the window size after each command and, if necessary,
    # update the values of LINES and COLUMNS.
    shopt -s checkwinsize

    # If set, the pattern "**" used in a pathname expansion context will
    # match all files and zero or more directories and subdirectories.
    shopt -s globstar
}

setup_colour_prompt()
{
    C_PRI='\[\033[0;33;1m\]'
    C_SEC='\[\033[0;35;1m\]'
    C_PNC='\[\033[0;31m\]'
    C_RST='\[\033[0m\]'

    export PS1="\
${C_PNC}[${C_SEC}\j${C_PNC}] \
${C_PRI}\u${C_PNC}@${C_SEC}\h\
${C_PNC} ${C_PRI}\w\
${C_RST} "

    unset C_PRI C_SEC C_PNC C_RST
}

setup_aliases()
{
    # My fave editor
    alias e='nvim'

    # Colours for LS and Grep by default.
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'

    alias ll='ls -l'
    alias la='ls -A'
    alias lsz='du -hd0'
}

# Determine whether terminal is interactive
case "$-" in
    *i*) INTERACTIVE=yes ;;
      *) INTERACTIVE=no  ;;
esac

# Determine whether terminal is coloured
case "$TERM" in
    foot|screen|*-256color|*-color) COLOURFUL=yes ;;
                                 *) COLOURFUL=no  ;;
esac

# Perform appropriate setup
if [ "$INTERACTIVE" = yes ];
then
    setup_shell
    setup_aliases

    setup_colour_prompt

    if [ "$COLOURFUL" = yes ]
    then
        setup_colour_prompt
    fi
fi

# Clean up after ourselves
unset    INTERACTIVE COLOURFUL
unset -f setup_aliases setup_colour_prompt setup_shell
