#!/bin/env python3

import sys, os
import scipy

def hex_to_rgb(h):
    return tuple(float(int(h[i:i+2], base=16)) for i in (0, 2, 4))

def rgb_to_hex(rgb):
    return "".join(hex(int(round(x)))[2:].zfill(2) for x in rgb)

# Colours to use to define the colourspace
COLOURS_TO_BASE      = list(range(0, 16))
# Colours to map into the space
COLOURS_TO_TRANSFORM = list(range(16, 256))

# Get a colourspace before the transform
COLOURS_UNTRANSFORMED = {}
for line in open("256-colors-table").readlines():
    index, r, g, b = line.split("\t")
    COLOURS_UNTRANSFORMED[int(index)] = tuple(float(x) for x in (r, g, b))

# Get initially transformed colourspace
COLOURS_TRANSFORMED = {}
for ind in COLOURS_TO_BASE:
    COLOURS_TRANSFORMED[ind] = hex_to_rgb(os.getenv(f"TERM_color{ind}"))

# Define a piecewise linear interpolator in 3D
interpolator = scipy.interpolate.LinearNDInterpolator(
    [COLOURS_UNTRANSFORMED[i] for i in COLOURS_TO_BASE],
    [COLOURS_TRANSFORMED[i]   for i in COLOURS_TO_BASE]
)

# Transform and print all the colours
for ind in COLOURS_TO_TRANSFORM:
    interp = interpolator(COLOURS_UNTRANSFORMED[ind])
    print(f"  {ind}={rgb_to_hex(interp)}")
