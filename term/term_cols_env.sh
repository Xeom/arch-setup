#!/bin/bash
#
# We store the terminal colours as an xresources file, so we can use the
# terminal.sexy website to import, edit and export them, and so we can easily
# share and use new colourschemes.
#
# This script will generate a .env file exporting values for the colours.

THIS_DIR=$(realpath $(dirname -- "$0"))

cat "$THIS_DIR/xresources" | while read -r LINE
do
    case $LINE
    in
        *.*:*)
            NAME=$(echo "$LINE" | cut -d: -f1 | cut -d. -f2)
            VAL=$(echo "$LINE" | cut -d: -f2 | cut -d'#' -f2)
            echo "export TERM_${NAME}='$VAL'"
            ;;
        *)
            ;;
    esac
done
