#!/bin/bash
#
# Generate the [colors] section of the foot.ini file.

THIS_DIR=$(realpath $(dirname -- "$0"))
source <($THIS_DIR/term_cols_env.sh)

echo "[colors]"

# Print the special colours
echo "  background=${TERM_background}"
echo "  foreground=${TERM_foreground}"

# Print the non-bright colours
for i in {0..7}
do
    NAME="TERM_color${i}"
    echo "  regular${i}=${!NAME}"
done

# Print the bright colours
for i in {0..7}
do
    NAME="TERM_color$(( ${i} + 8 ))"
    echo "  bright${i}=${!NAME}"
done

# Print interpolated colours (for 256 colour)
python3 interpolate_ini_cols.py
