#!/bin/bash
#
# Update the [colors] section of the foot.ini file.

THIS_DIR=$(realpath $(dirname -- "$0"))

update_colors_file()
{
    # ARGUMENTS ...
    local FILE=$1 # The filename to update

    sed -i '/^\[colors\]$/Q' "$FILE"
    "$THIS_DIR/generate_ini_cols.sh" >> "$FILE"
}

# Update the foot.ini file.
update_colors_file "$THIS_DIR/foot.ini"
