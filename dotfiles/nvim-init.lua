-- Load my normal VIM config
vim.cmd("set runtimepath^=~/.vim runtimepath+=~/.vim/after")
vim.cmd("let &packpath = &runtimepath")
vim.cmd("source ~/.vimrc")

local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not vim.loop.fs_stat(lazypath) then
    local lazyrepo = 'https://github.com/folke/lazy.nvim.git'
    vim.fn.system {
        'git', 'clone', '--filter=blob:none', '--branch=stable',
        lazyrepo, lazypath
    }
end
vim.opt.rtp:prepend(lazypath)

local add_lsp_key_bindings = function(buf)
    local map = function(keys, func, desc)
        local binding = '<Leader>' .. keys
        vim.keymap.set('n', binding, func, { buffer = buf, desc = desc })
    end

    local telescope = require('telescope.builtin')

    map('gd', telescope.lsp_definitions, '[g]oto [d]efinition')
    map('gr', telescope.lsp_references, '[g]oto [r]eferences')
    map('gi', telescope.lsp_implementations, '[g]oto [i]mplementation')
    map('gt', telescope.lsp_type_definitions, '[g]oto [t]ype')
    map('?', vim.lsp.buf.hover, 'Documentation [?]')
    map('gD', vim.lsp.buf.declaration, '[g]oto [D]eclaration')
end

require('lazy').setup({
    {
        'nvim-telescope/telescope.nvim',
        event = 'VimEnter',
        dependencies = { 'nvim-lua/plenary.nvim' }
    },
    { -- LSP Configuration & Plugins
        'neovim/nvim-lspconfig',
        dependencies =
        {
            { 'williamboman/mason.nvim', config = true },
            'williamboman/mason-lspconfig.nvim',
            'WhoIsSethDaniel/mason-tool-installer.nvim',
        },
        config = function()
            vim.api.nvim_create_autocmd(
                'LspAttach',
                {
                    group = vim.api.nvim_create_augroup(
                        'lsp-controls', { clear = true }
                    ),
                    callback = function(evt)
                        -- Stop LSP from trying to highlight anything
                        local cli = vim.lsp.get_client_by_id(evt.data.client_id)
                        cli.server_capabilities.semanticTokensProvider = nil
                        add_lsp_key_bindings(evt.buffer)
                    end,
                }
            )

            require('mason').setup()
            require('mason-tool-installer').setup {
                ensure_installed = { 'clangd', 'rust_analyzer' }
            }
            require('mason-lspconfig').setup {
                handlers = {
                    function(name)
                        require('lspconfig')[name].setup({})
                    end,
                },
            }
        end,
    }
})
