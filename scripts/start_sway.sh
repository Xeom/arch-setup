#!/bin/bash

# Ensure backlight is not zero!
light -S 50
light -N 10

XDG_RUNTIME_DIR=/home/fwharf/.xdg-runtime \
	dbus-run-session sway 2>&1 > sway-log
