#!/bin/bash

TERM=foot

POPUP_WIDTH=${POPUP_WIDTH:-100}
POPUP_HEIGHT=${POPUP_HEIGHT:-40}

ARGS=()

usage()
{
    echo "$0: Open a popup-terminal"
    echo ""
    echo "Options:"
    echo "  -w [N] Terminal width in characters"
    echo "  -h [N] Terminal height in characters"
    echo "  -f [N] Terminal font"
}

while getopts "w:h:f:" OPT
do
    case "${OPT}" in
        w ) POPUP_WIDTH="${OPTARG}"  ;;
        h ) POPUP_HEIGHT="${OPTARG}" ;;
        f ) ARGS+=("-f" "${OPTARG}") ;;
        \?) usage ; exit 1           ;;
        * )
    esac
done

ARGS+=("-T" "Term-Popup")
ARGS+=("-W" "${POPUP_WIDTH}x${POPUP_HEIGHT}")
ARGS+=("--" $*)

echo "${ARGS[@]}"
exec $TERM "${ARGS[@]}"
