#!/bin/bash

LOCATION="Chandler's%20Ford"
DOMAIN="wttr.in"

PAGE_URIS=(
    "$DOMAIN/$LOCATION"
    "$DOMAIN/Moon"
    "v2.$DOMAIN/$LOCATION"
)

EMOJIS_REM_SP=$'\u2601\ufe0f|\u2744\ufe0f|\u2600\ufe0f'
EMOJIS_ADD_SP=$'\U0001f327|\U0001f328|\U0001f329'

fix_emoji_widths()
{
    cat |
        sed -Ee "s/(${EMOJIS_REM_SP}) (.)/\\1\\2/g" \
            -Ee "s/(${EMOJIS_ADD_SP})/\\1  /g"
}

show_page()
{
    local PAGE="$1"
    local URI="${PAGE_URIS[PAGE]}"
    clear_screen
    curl -s --compressed "${URI}" | fix_emoji_widths
}

clear_screen()
{
    printf '\033[2J\033[H'
}

show_page 0

while read
do
    show_page 0
done

