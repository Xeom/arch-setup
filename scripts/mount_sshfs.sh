#!/bin/bash

is_mounted()
{
    # ARGUMENTS ...
    local MOUNTPOINT="$1" # The mountpoint to check.

    mount | cut -d' ' -f3 | grep -q "$MOUNTPOINT"
}


mount_sshfs()
{
    # ARGUMENTS ...
    local MOUNTPOINT="$1" # The mountpoint.
    local REMOTE="$2"     # The remote to mount.

    [ -d "$MOUNTPOINT" ] || mkdir -p "$MOUNTPOINT"

    is_mounted "$MOUNTPOINT" || sshfs "$REMOTE" "$MOUNTPOINT" \
        -o idmap=user \
        -o compression=yes \
        -o reconnect
}

mount_sshfs $HOME/www tiramisusan:/home/francis

