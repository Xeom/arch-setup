#!/bin/bash

SOURCE=https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

install_in_file()
{
    curl -fLo "$1" --create-dirs "$SOURCE"
}

install_in_file ~/.config/nvim/autoload/plug.vim
install_in_file ~/.vim/autoload/plug.vim

