#!/bin/bash

set -o pipefail

if [ -t 1 ]
then
    TMPFILE=$(mktemp /tmp/cargo_less-XXXXXX)
    if cargo --color=always "$@" 2>&1 | tee $TMPFILE
    then
        rm "$TMPFILE"
    else
        less -R "$TMPFILE"
        rm "$TMPFILE"
        exit 1
    fi
else
    exec cargo "$@"
fi
