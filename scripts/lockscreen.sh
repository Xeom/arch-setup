#!/bin/bash

. <(term_cols_env.sh)

TEXT_COLOUR=${TERM_color13}
BG_COLOUR=${TERM_background}
LINE_COLOUR=${TERM_color0}
RING_COLOUR=${TERM_color9}
CLEAR_COLOUR=${TERM_color11}
VERIFY_COLOUR=${TERM_color12}
WRONG_COLOUR=${TERM_color1}
CLEAR_COLOUR=${TERM_color11}

dim()
{
    local NAME="TERM_color${1}"
    echo ${!NAME}
}

bright()
{
    local NAME="TERM_color$((8+$1))"
    echo ${!NAME}
}

set_state_colour()
{
    local STATE=$1
    local COLOUR=$2
    OPTIONS+=(
        "--ring-${STATE}color"   "$(dim $COLOUR)"
        "--line-${STATE}color"   "$(dim 0)"
        "--inside-${STATE}color" "${TERM_background}"
        "--text-${STATE}color"   "$(bright $COLOUR)"
    )
}

OPTIONS=(
    -f
    --clock --indicator --font 'Source Code Pro'
    --screenshot
    --effect-blur 16x5
    --key-hl-color $(bright 3)
    --separator-color $(dim 0)
)

set_state_colour ""       "5"
set_state_colour "clear-" "3"
set_state_colour "ver-"   "2"
set_state_colour "wrong-" "1"

GRACE=y

for ARG in "$@"
do
    case "$ARG" in
        --immediate)
            GRACE=n
            ;;
        *)
            echo Unrecognised argument "$ARG" 1>&2
            exit 1
            ;;
    esac
done

[[ "$GRACE" = "y" ]] && OPTIONS+=(--grace 10 --fade-in 0.5)

exec swaylock "${OPTIONS[@]}"
