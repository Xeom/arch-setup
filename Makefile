# _/ DOTFILES \_________________________________________________________________

# ___/ Simple dotfiles! \___
#  These dotfiles are simply linked to the destination
define DO_SIMPLE_DOTFILE
${HOME}/$(2): $(1)
	@echo === Placing $(1) ===
	if [ ! -d $$(@D) ]; then mkdir -p $$(@D); fi
	ln -sf $$(realpath $(1)) $$@
TGTS_DOTFILES += ${HOME}/$(2)
endef

# ___/ Generated dotfiles! \___
#  These dotfiles are generated as then output of a script
define DO_GENERATED_DOTFILE
${HOME}/$(2): $(1)
	@echo === Generating $(1) ===
	if [ ! -d $$(@D) ]; then mkdir -p $$(@D); fi
	if [ -f $$@ ]; then rm $$@; fi
	bash $(1) > $$@
TGTS_DOTFILES += ${HOME}/$(2)
endef

$(eval $(call DO_SIMPLE_DOTFILE, dotfiles/vimrc,.vimrc))
$(eval $(call DO_SIMPLE_DOTFILE, dotfiles/nvim-init.lua,.config/nvim/init.lua))
$(eval $(call DO_SIMPLE_DOTFILE, dotfiles/zathurarc,.config/zathura/zathurarc))
$(eval $(call DO_SIMPLE_DOTFILE, dotfiles/gitconfig,.gitconfig))
$(eval $(call DO_SIMPLE_DOTFILE, dotfiles/ssh,.ssh/config))
$(eval $(call DO_SIMPLE_DOTFILE, dotfiles/latexmkrc,.latexmkrc))
$(eval $(call DO_SIMPLE_DOTFILE, term/foot.ini,.config/foot/foot.ini))
$(eval $(call DO_SIMPLE_DOTFILE, bash/bash_profile.sh,.bash_profile))
$(eval $(call DO_SIMPLE_DOTFILE, sway/bar/divebar-conf.toml,.divebar.toml))

$(eval $(call DO_GENERATED_DOTFILE, bash/gen_bashrc.sh,.bashrc))
$(eval $(call DO_GENERATED_DOTFILE, tmux/gen_tmuxconf.sh,.tmux.conf))
$(eval $(call DO_GENERATED_DOTFILE, sway/gen_swayconf.sh,.config/sway/config))

# _/ DEFAULT TARGETS \__________________________________________________________

all: $(TGTS_DOTFILES)
.DEFAULT_GOAL=all
